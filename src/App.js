import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Slide from '@material-ui/core/Slide';
import TodosLayout from './layouts/TodosLayout';
import { makeStyles } from '@material-ui/core/styles';
import './App.css';


const useStyles = makeStyles(theme => ({
  header: {
    background: '#f5f5f5',
  },
  typography: {
    maxWidth: '600px',
    width: '100%',
    margin: '0 auto',
    padding: '0 8px',
    color: "#8c8ca2",
    fontWeight: "900",
    textAlign: 'center',
    textShadow: "0px 2px 2px #fff",
    [theme.breakpoints.up('sm')]: {
      padding: '0 24px'
    },
  },
  container: {
    paddingTop: "56px",
  }
}));

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });
  return (
    <Slide appear={false} direction='down' in={!trigger}>
      {children}
    </Slide>
  );
}

const App = (props) => {
  const classes = useStyles();

  return (
    <Fragment>
      <HideOnScroll {...props}>
        <AppBar className={classes.header}>
          <Toolbar>
            <Typography className={classes.typography} variant='h4'>Todo List</Typography>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Container className={classes.container} maxWidth='sm'>
        <Box my={2}>
          <TodosLayout />
        </Box>
      </Container>
    </Fragment>
  );
}

export default App;

App.propTypes = {
  classes: PropTypes.object,
  children: PropTypes.element,
  window: PropTypes.func
};