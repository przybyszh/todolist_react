import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import { withStyles } from '@material-ui/styles';

const styles = {
  ul: {
    margin: '0',
    padding: '0'
  }
};

const List = (props) => {
  const { classes, items, handleRemoveTask, handleChangeTask } = props;

  return (
    <ul className={classes.ul}>
      {items.map(task => (
        <ListItem
          key={task.id}
          item={task}
          handleRemoveTask={handleRemoveTask}
          handleChangeTask={handleChangeTask}
        />
      ))}
    </ul>
  )
}

export default withStyles(styles)(List);

List.propTypes = {
  classes: PropTypes.object,
  items: PropTypes.array,
  handleRemoveTask: PropTypes.func.isRequired,
  handleChangeTask: PropTypes.func.isRequired,
};