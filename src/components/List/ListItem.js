import React from 'react';
import PropTypes from 'prop-types';
import Input from '../Input/Input';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/styles';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import Zoom from '@material-ui/core/Zoom';

const styles = {
  paper: {
    width: '100%',
    padding: "10px",
    margin: "0",
    cursor: 'pointer'
  },
  li: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '15px',
  },
  buttonDelete: {
    flex: 'none',
    width: '46px',
    height: '46px',
    margin: '0 0 0 -23px'
  },
  text: {
    display: 'block',
    cursor: 'pointer'
  }
};

class ListItem extends React.Component {

  state = {
    editMount: false,
    value: this.props.item.title,
    errorEdit: false,
  }

  toggleEditMode = () => {
    this.setState({ editMount: !this.state.editMount })
  }

  handleChangeInput = (e) => {
    this.setState({ value: e.target.value })
  }

  handleChangeItem = (e) => {
    e.preventDefault();
    const { value } = this.state
    if (value === '') {
      this.setState({ errorEdit: true })
    } else {
      const { item, handleChangeTask } = this.props
      this.setState({ errorEdit: false })
      handleChangeTask(item.id, value);
      this.toggleEditMode();
    }
  }

  render() {
    const { value, errorEdit, editMount } = this.state;
    const { classes, item, handleRemoveTask } = this.props;

    return (
      <li className={classes.li}>

        <Paper className={classes.paper}>
          {editMount ? (
            <form onSubmit={this.handleChangeItem}>
              <Input
                autoFocus
                error={errorEdit}
                value={value}
                onChange={this.handleChangeInput}
              />
            </form>
          ) : (
              <Tooltip title="Edit" placement="top-start">
                <span className={classes.text} onClick={this.toggleEditMode}>
                  {item.title}
                </span>
              </Tooltip>
            )}
        </Paper>



        <Tooltip title="Delete" placement="left" TransitionComponent={Zoom}>
          <Fab
            className={classes.buttonDelete}
            type="button"
            color="secondary"
            aria-label="Add"
            onClick={() => handleRemoveTask(item.id)}
          >
            <DeleteIcon />
          </Fab>
        </Tooltip>

      </li>
    )
  }
}

export default withStyles(styles)(ListItem);

ListItem.propTypes = {
  classes: PropTypes.object,
  item: PropTypes.object,
  handleRemoveTask: PropTypes.func.isRequired,
};