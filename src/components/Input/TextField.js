import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles(theme => ({
  textField: {
    margin: "0",
    width: "100%",
  }
}));


const TextFeild = (props) => {
  const classes = useStyles();
  return (
    <TextField
      {...props}
      error={props.error}
      // helperText={props.error ? 'Empty field!' : ''}
      id="standard-search"
      label="Task field"
      type="text"
      className={classes.textField}
      margin="normal"
    />
  )
}

export default TextFeild;

TextFeild.propTypes = {
  error: PropTypes.bool,
  classes: PropTypes.object,
};