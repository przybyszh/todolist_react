import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import TextFeild from './TextField';

const Input = ({ type, labelText, ...props }) => {
  const childrenProps = { type, ...props };

  const renderInputField = () => {
    switch (type) {
      default:
        return <TextFeild {...childrenProps} />
    }
  };

  return (
    <Fragment>
      {renderInputField()}
    </Fragment>
  )
}

export default Input;

Input.propTypes = {
  type: PropTypes.string,
};