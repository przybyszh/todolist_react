import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Input from '../components/Input/Input';
import List from '../components/List/List';
import Task from '../models/Task';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/styles';
import Zoom from '@material-ui/core/Zoom';

const styles = {
  form: {
    display: 'flex',
    alignItems: 'flex-end'
  },
  buttonAdd: {
    flex: 'none',
    width: '46px',
    height: '46px',
    margin: '0 0 0 -23px'
  }
};

class TodosLayout extends Component {
  state = {
    newTodo: '',
    listTodo: [],
    error: false
  }

  componentDidMount() {
    const storage = JSON.parse(localStorage.getItem('listTodo'));
    if (storage) {
      this.setState({ listTodo: storage })
    }
  }

  handleChangeInput = (e) => {
    if (e.target.value === '') {
      this.setState({ error: false })
    } else {
      this.setState({
        newTodo: e.target.value,
        error: false
      })
    }
  }

  handleSubmintForm = (e) => {
    e.preventDefault();
    if (this.state.newTodo === '') {
      this.setState({ error: true })
    } else {
      const task = new Task(this.state.newTodo);
      process.nextTick(() => {
        this.setState({
          listTodo: [task, ...this.state.listTodo],
          newTodo: '',
          error: false
        })
        localStorage.setItem('listTodo', JSON.stringify(this.state.listTodo));
      })
    }
  }

  handleRemoveTask = (id) => {
    const { listTodo } = this.state;
    const filterList = listTodo.filter(task => task.id !== id);
    this.setState({ listTodo: filterList })
    localStorage.setItem('listTodo', JSON.stringify(filterList));
  }

  handleChangeTask = (id, title) => {
    const { listTodo } = this.state;
    const editList = listTodo.map(task => {
      if (task.id === id) {
        task.title = title
      }
      return task;
    })
    this.setState({
      listTodo: editList,
      error: false
    });
    localStorage.setItem('listTodo', JSON.stringify(editList));
  }

  render() {
    const { classes } = this.props;
    const { newTodo, listTodo, error } = this.state;
    const btnError = error ? 'secondary' : 'primary';

    return (
      <Fragment>
        <List
          items={listTodo}
          handleRemoveTask={this.handleRemoveTask}
          handleChangeTask={this.handleChangeTask}
        />
        <form className={classes.form} onSubmit={this.handleSubmintForm}>
          <Input error={error} onChange={this.handleChangeInput} value={newTodo} type="text" autoComplete='off' placeholder="type a new task" />

          <Tooltip title="Add" placement="left" TransitionComponent={Zoom}>
            <Fab className={classes.buttonAdd} type="submit" color={btnError} aria-label="Add"> <AddIcon /> </Fab>
          </Tooltip>

        </form>
      </Fragment>
    )
  }
}

export default withStyles(styles)(TodosLayout);

TodosLayout.propTypes = {
  classes: PropTypes.object
};